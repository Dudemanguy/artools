#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_CI_SH:-} ]] || return 0
ARTOOLS_INCLUDE_CI_SH=1

set -e

artixpkg_ci_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [COMMAND] [OPTIONS]

    COMMANDS
        config         Configure ci and build agent

    OPTIONS
        -h, --help     Show this help text

    EXAMPLES
        $ ${COMMAND} config libfoo
_EOF_
}

artixpkg_ci() {
    if (( $# < 1 )); then
        artixpkg_ci_usage
        exit 0
    fi

    # option checking
    while (( $# )); do
        case $1 in
        -h|--help)
            artixpkg_ci_usage
            exit 0
        ;;
        config)
            _ARTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/ci/config.sh
            source "${LIBDIR}"/pkg/ci/config.sh
            artixpkg_ci_config "$@"
            exit 0
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            die "invalid command: %s" "$1"
        ;;
        esac
    done
}
