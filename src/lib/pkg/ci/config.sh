#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_CI_CONFIG_SH:-} ]] || return 0
ARTOOLS_INCLUDE_CI_CONFIG_SH=1

# shellcheck source=src/lib/pkg/db/db.sh
source "${LIBDIR}"/pkg/db/db.sh

set -e


write_jenkinsfile() {
    printf "@Library('artix-ci@%s') import org.artixlinux.RepoPackage\n" "${1}" > "${REPO_CI}"
    {
        printf '\n'
        printf 'PackagePipeline(new RepoPackage(this))\n'
    } >> "${REPO_CI}"
}

artixpkg_ci_config_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        -a, --agent NAME       Set the CI agent (default: ${AGENTS[0]})
                               Possible values: $(yaml_array ${AGENTS[@]})
        -s, --switch           Switch agent
        -j, --jobs N           Run up to N jobs in parallel (default: $(nproc))
        -h, --help             Show this help text

    EXAMPLES
        $ ${COMMAND} --agent ${AGENTS[1]} libfoo
        $ ${COMMAND} --switch --agent ${AGENTS[1]} libfoo
        $ ${COMMAND} *
_EOF_
}


artixpkg_ci_config() {
    # options
    local jobs=
    jobs=$(nproc)
    local paths=()

    local AGENT=${AGENTS[0]}
    local SWITCH=0

    # variables
    local RUNCMD=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    local path realpath pkgbase

    while (( $# )); do
        case $1 in
        -h|--help)
            artixpkg_ci_config_usage
            exit 0
        ;;
        -s|--switch)
            SWITCH=1
            shift
        ;;
        -a|--agent)
            (( $# <= 1 )) && die "missing argument for %s" "$1"
            AGENT="$2"
            RUNCMD+=" $1 ${AGENT}"
            shift 2
        ;;
        -j|--jobs)
            (( $# <= 1 )) && die "missing argument for %s" "$1"
            jobs=$2
            shift 2
        ;;
        --)
            shift
            break
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            paths=("$@")
            break
        ;;
        esac
    done

    # check if invoked without any path from within a packaging repo
    if (( ${#paths[@]} == 0 )); then
        if [[ -f PKGBUILD ]]; then
            paths=(".")
        else
            artixpkg_ci_config_usage
            exit 1
        fi
    fi

    # parallelization
    if [[ ${jobs} != 1 ]] && (( ${#paths[@]} > 1 )); then
        if [[ -n ${BOLD} ]]; then
            export ARTOOLS_COLOR=always
        fi
        if ! parallel --bar --jobs "${jobs}" "${RUNCMD}" ::: "${paths[@]}"; then
            die 'Failed to configure some packages, please check the output'
            exit 1
        fi
        exit 0
    fi

    for path in "${paths[@]}"; do
        if ! realpath=$(realpath -e "${path}"); then
            error "No such directory: ${path}"
            continue
        fi

        pkgbase=$(basename "${realpath}")
        pkgbase=${pkgbase%.git}

        if [[ ! -d "${path}/.git" ]]; then
            error "Not a Git repository: ${path}"
            continue
        fi
        ( cd "${path}" || return

            if [[ ! -f ${REPO_CI} ]]; then

                [[ -d .artixlinux ]] || mkdir .artixlinux

                msg "Adding ci support ..."
                write_jenkinsfile "${AGENT}"

                git add "${REPO_CI}"
                git commit -m "add ci support"
            fi

            if [[ ! -f ${REPO_DB} ]]; then

                msg "Creating repo db ..."
                create_repo_db

                if [[ -f PKGBUILD ]]; then
                    # shellcheck source=contrib/makepkg/PKGBUILD.proto
                    source PKGBUILD
                    update_yaml_base
                fi
                git add "${REPO_DB}"
                git commit -m "create repo db"
            fi

            if (( SWITCH )); then
                msg "Switching to agent (${AGENT}) ..."
                write_jenkinsfile "${AGENT}"

                git add "${REPO_CI}"
                git commit -m "switch agent"
            fi

        )
    done
}
