#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_REPO_ADD_SH:-} ]] || return 0
ARTOOLS_INCLUDE_REPO_ADD_SH=1

set -e


check_pkgbuild_validity() {
    # skip when there are no sources available
    if (( ! ${#source[@]} )); then
        return
    fi

    # validate sources hash algo is at least > sha1
    local bad_algos=("cksums" "md5sums" "sha1sums")
    local good_hash_algo=false

    # from makepkg libmakepkg/util/schema.sh
    for integ in "${known_hash_algos[@]}"; do
        local sumname="${integ}sums"
        if [[ -n ${!sumname} ]] && ! in_array "${sumname}" "${bad_algos[@]}"; then
            good_hash_algo=true
            break
        fi
    done

    if ! $good_hash_algo; then
        die "PKGBUILD lacks a secure cryptographic checksum, insecure algorithms: ${bad_algos[*]}"
    fi
}

artixpkg_repo_add_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [DEST_REPO] [PKGBASE]...

    OPTIONS
        -m, --manual            Disable auto repo
                                Possible auto values: $(yaml_array ${ARTIX_DB_MAP[@]})
        -p, --push              Push pkgbase
        -r, --rebuild           Triggers a rebuild
        -n, --nocheck           Disable the check function
        -h, --help              Show this help text

    EXAMPLES
        $ ${COMMAND} ${ARTIX_DB[4]} libfoo
        $ ${COMMAND} --push ${ARTIX_DB[4]} libfoo
        $ ${COMMAND} --auto --push ${ARTIX_DB_MAP[2]} libfoo
_EOF_
}

artixpkg_repo_add() {
    if (( $# < 1 )); then
        artixpkg_repo_add_usage
        exit 0
    fi

    # options
    local pkgbases=()
    local pkgbase

    local PUSH=0
    local DEST=''
    local REBUILD=0
    local NOCHECK=0
    local ADD=1
    local AUTO=1

    while (( $# )); do
        case $1 in
            -h|--help)
                artixpkg_repo_add_usage
                exit 0
            ;;
            -m|--manual)
                AUTO=0
                shift
            ;;
            -p|--push)
                PUSH=1
                shift
            ;;
            -r|--rebuild)
                REBUILD=1
                shift
            ;;
            -n|--nocheck)
                NOCHECK=1
                shift
            ;;
            -*)
                die "invalid argument: %s" "$1"
            ;;
            *)
                break
            ;;
        esac
    done

    DEST="$1"
    shift
    pkgbases+=("$@")

    if (( AUTO )); then
        if ! in_array "${DEST}" "${ARTIX_DB_MAP[@]}"; then
            die "${DEST} does not exist!"
        fi
    else
        if ! in_array "${DEST}" "${ARTIX_DB[@]}"; then
            die "${DEST} does not exist!"
        fi
    fi

    for pkgbase in "${pkgbases[@]}"; do

        if [[ -d "${pkgbase}" ]];then

            if [[ ! -d "${pkgbase}/.git" ]]; then
                error "Not a Git repository: ${pkgbase}"
                continue
            fi
            ( cd "${pkgbase}" || return

                if ! has_remote_changes; then

                    if [[ ! -f PKGBUILD ]]; then
                        die "No PKGBUILD found in (%s)" "${pkgbase}"
                    fi

                    # shellcheck source=contrib/makepkg/PKGBUILD.proto
                    source PKGBUILD

                    check_pkgbuild_validity

                    manage-pkgbuild-keys --export

                    update_yaml_base

                    local auto
                    auto=$(auto_detect)

                    if [[ -z "${auto}" ]]; then
                        auto=$(team_from_yaml)
                    fi

                    if (( AUTO )); then
                        if [[ "${DEST}" == "${ARTIX_DB_MAP[2]}" ]]; then
                            DEST="${auto}"
                        else
                            DEST="${auto}-${DEST}"
                        fi
                    fi

                    update_yaml_add "${REBUILD}" "${ADD}" "${NOCHECK}" "${DEST}"

                    update_yaml_team "${auto}"


                    local commit_msg
                    commit_msg=$(get_commit_msg 'add' "${DEST}")

                    if [[ -f .SRCINFO ]]; then
                        rm .SRCINFO
                    fi

                    delete_obsolete_map_keys

                    if [[ -n $(git status --porcelain --untracked-files=no) ]]; then

                        stat_busy 'Staging files'
                        for f in $(git ls-files --others); do
                            git add "$f"
                        done
                        for f in $(git ls-files --modified); do
                            git add "$f"
                        done
                        for f in $(git ls-files --deleted); do
                            git rm "$f"
                        done
                        stat_done

                        msg 'Commit'
                        git commit -m "${commit_msg}"

                        if (( PUSH )); then
                            msg "Push (${pkgbase})"
                            git push origin master
                        fi

                        msg "Querying ${pkgbase} ..."
                        if ! show_db; then
                            warning "Could not query ${REPO_DB}"
                        fi
                    fi

                fi
            )
        fi

    done
}
