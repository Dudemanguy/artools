#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

LIBDIR=${LIBDIR:-'@libdir@'}

# shellcheck source=src/lib/base/message.sh
source "${LIBDIR}"/base/message.sh
# shellcheck source=src/lib/base/chroot.sh
source "${LIBDIR}"/base/chroot.sh
# shellcheck source=src/lib/pkg/deploy.sh
source "${LIBDIR}"/pkg/deploy.sh

passfiles=("$@")

sign_pkg(){
    local file_to_sign="$1"

    if [ ! -e "$file_to_sign" ]; then
        error "%s does not exist!" "$file_to_sign"
        exit 1
    fi

    if [[ -n "${GPG_KEY}" ]] && [[ -n "${GPG_PASS}" ]]; then
        msg "Signing [%s]" "${file_to_sign##*/}"
        if ! gpg --homedir /etc/pacman.d/gnupg --no-permission-warning \
            --no-default-keyring --default-key "${GPGP_KEY}" --passphrase "${GPGP_PASS}" \
            -q --detach-sign "$file_to_sign"; then
           return 1
        fi
    fi
    return 0
}

check_root "" "${BASH_SOURCE[0]}" "$@"

for pkg in "${passfiles[@]}"; do
    msg "Searching %s ..." "$pkg"
    if pkgfile=$(find_cached_pkgfile "$pkg");then
        msg2 "Found: %s" "${pkgfile}"
        [[ -e "${pkgfile}".sig ]] && rm "${pkgfile}".sig
        if ! sign_pkg "${pkgfile}"; then
            die "Failed to sign package!"
        fi
        ret=0
    else
        ret=1
    fi
done

exit "$ret"
